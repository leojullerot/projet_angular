export class Product {
    id: number;
    brand: string;
    type: string;
    price: number;
    stock: number;
    reference: string;
}

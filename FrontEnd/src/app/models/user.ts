export class User {
    id: number;
    lastname: string;
    firstName: string;
    adress: string;
    postalCode: number;
    city: string;
    phone: string;
    email: string;
    sex: string;
    login: string;
    password: string;
    passwordcheck: string;
}

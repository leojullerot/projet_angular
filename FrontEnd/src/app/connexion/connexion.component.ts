import { Component, OnInit, ɵɵcontainerRefreshEnd } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { AuthentificationService } from '../service/authentification.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {

  login: any;
  password: any;

  constructor(
    private formBuilder: FormBuilder,
    private authentificationService: AuthentificationService,
    private router: Router,
  ) { }

  ngOnInit() {
  }

  connect() {
    console.log(this.login);
    console.log(this.password);

    this.authentificationService.login(this.login, this.password).subscribe(success => {
      localStorage.setItem('jwt_token', success.jwt_token);

    });
    console.log(localStorage.getItem('jwt_token'));
  }
}

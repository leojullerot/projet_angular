import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpHeaders } from '@angular/common/http';
import { environment } from 'src/environments/environment';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AuthentificationService {

  baseUrl = environment.backend;
  constructor(private http: HttpClient) {
  }

  login(loginP: string, passwordP: string ): Observable<any> {
    const loginModel = { login: loginP, password: passwordP};
    // return this.http.post(`${this.baseUrl}/login`, {login, password, observe: 'response'});
    return this.http.post(`${this.baseUrl}/signin`, loginModel);
  }

  getToken() {
    return localStorage.getItem('jwt_token');
  }

  isLoggedIn(): boolean {
    const authToken = localStorage.getItem('jwt_token');
    return (authToken !== null);
  }

}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';


import { Observable } from 'rxjs';
import { Product } from '../models/product';
import { environment } from '../../environments/environment';

import { tap, map } from 'rxjs/operators';
import {User} from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  constructor(private http: HttpClient) { }
    public getProducts(): Observable<Product[]> {
        return this.http.get<Product[]>(environment.backendProduct);
    }

    getProduct(): Observable<Product[]> {
      return this.http.get<Product[]> ( environment.backendProduct).pipe (tap((value) => console.log(value)) );
    }

  // tslint:disable-next-line:ban-types
    public getOneProduct(name: String): Observable<Product[]> {
      // tslint:disable-next-line:triple-equals no-shadowed-variable
      return  this.http.get<Product[]> (environment.backendProduct).pipe(map(p => p.filter(p => p.type == name)));
    }

  public setUser(user: User): Observable<any>  {
    console.log(environment.backend);
    // tslint:disable-next-line:max-line-length
    const modelUser = {
      login: user.login,
      password: user.password,
      city: user.city,
      email: user.email,
      firstname: user.firstName,
      lastname: user.lastname,
      address: user.adress,
      postalCode: user.postalCode,
      phone: user.phone
    };
    return this.http.post<any>(environment.backend + '/signup', modelUser);
  }

  getUser() {
    const jwtToken = {jwt_token: localStorage.getItem('jwt_token')};
    return this.http.post<any>(environment.backend + '/user', jwtToken);
  }
}

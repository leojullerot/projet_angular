import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import { ApiService } from '../service/api.service';
import { Observable } from 'rxjs';
import { Product } from '../models/product';
import { DelProduct } from '../store/actions/product.action';
import * as angular from 'angular';
import {tap} from 'rxjs/operators';


@Component({
  selector: 'app-panier',
  templateUrl: './panier.component.html',
  styleUrls: ['./panier.component.css']
})
export class PanierComponent implements OnInit {

  catalogue: Observable<Product[]>;
  totalPrice: number;

  constructor(private store: Store) {
    this.totalPrice  = 0;
    this.catalogue = this.store.select(state => state.product.panier);
    this.catalogue.subscribe(p => p.forEach(element => this.totalPrice += Number(element.price)));
    console.log(this.totalPrice);
   }

  ngOnInit() {
  }

  onClick(product) {
    this.deletProduct (product);
  }

  deletProduct(product) {
    this.store.dispatch(new DelProduct(product));
    this.totalPrice -= product.price;
  }
}


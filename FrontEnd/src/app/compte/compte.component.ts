import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { User } from '../models/user';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import {environment} from '../../environments/environment';
import {Product} from '../models/product';
import {ApiService} from '../service/api.service';

@Component({
  selector: 'app-compte',
  templateUrl: './compte.component.html',
  styleUrls: ['./compte.component.css']
})
export class CompteComponent implements OnInit {

  products: User[];
  filteredProducts: User[];


  compteForm: FormGroup;
  submit = false;
  validPassword = true;
  validCompte = false;
  user = new User();

  private url = environment.backend + '/signup';

  // tslint:disable-next-line:variable-name
  constructor(private formBuilder: FormBuilder, private http: HttpClient, private apiService: ApiService) { }


  ngOnInit() {
    this.compteForm = this.formBuilder.group({
      name: ['jullerot', Validators.required],
      firstName: ['leo', Validators.required],
      adress: ['12 rue maria callas', [Validators.required]],
      cp: ['67380', [Validators.required]],
      ville: ['Lingolsheim', [Validators.required]],
      tel: ['0628096540', Validators.compose([Validators.required, Validators.maxLength(10), Validators.minLength(10)])],
      email: ['leojullerot@outlook.fr', Validators.compose([Validators.email, Validators.required])],
      civilite: ['Homme', [Validators.required]],
      login: ['kingair', [Validators.required]],
      password: ['toto', [Validators.required]],
      passwordVerif: ['toto', [Validators.required]],
    },
    {});
  }

  get getCompte() {
    return this.compteForm.controls;
  }


  saveUser() {
    if (this.verifPassword()) {
      this.submit = true;
      this.validPassword = true;

      if (this.compteForm.valid) {
        this.validCompte = true;
        this.user.lastname = this.compteForm.controls.name.value;
        this.user.firstName = this.compteForm.controls.firstName.value;
        this.user.adress = this.compteForm.controls.adress.value;
        this.user.postalCode = this.compteForm.controls.cp.value;
        this.user.city = this.compteForm.controls.ville.value;
        this.user.phone = this.compteForm.controls.tel.value;
        this.user.email = this.compteForm.controls.email.value;
        this.user.sex = this.compteForm.controls.civilite.value;
        this.user.login = this.compteForm.controls.login.value;
        this.user.password = this.compteForm.controls.password.value;
        this.user.passwordcheck = this.compteForm.controls.passwordVerif.value;

        this.addUser(this.user);
      }
    } else {
      this.validPassword = false;
    }
  }
  verifPassword(): boolean {
    console.log(this.compteForm.controls.password.value === this.compteForm.controls.passwordVerif.value);
    return this.compteForm.controls.password.value === this.compteForm.controls.passwordVerif.value;
  }


  addUser(user: User) {

    this.apiService.setUser(user).subscribe();
  }
}

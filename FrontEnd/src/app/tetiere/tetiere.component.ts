import { Component, OnInit } from '@angular/core';
import { Store } from '@ngxs/store';
import {User} from '../models/user';
import {Product} from '../models/product';
import {ApiService} from '../service/api.service';
import {Router} from '@angular/router';


@Component({
  selector: 'app-tetiere',
  templateUrl: './tetiere.component.html',
  styleUrls: ['./tetiere.component.css']
})
export class TetiereComponent implements OnInit {
  connected: boolean;
  nbProduct: number;
  user: User;

  constructor( private apiService: ApiService, private store: Store) {
    this.store.select(state => state.product.panier).subscribe (u => this.nbProduct = u.length);
  }

  ngOnInit() {
    this.update();
  }

  onClick() {
    this.update();
  }

  onClickDisconect() {
    localStorage.setItem('jwt_token', null);
    this.update();
    console.log(localStorage.getItem('jwt_token'));
  }

  update() {
    this.connected = localStorage.getItem('jwt_token') !== 'null';
  }

  onClickCompte() {
    this.update();
    this.apiService.getUser().subscribe();
  }
}

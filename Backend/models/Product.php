<?php
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="products")
 */
class Product
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string")
     */
    protected $reference;

    /**
     * @ORM\Column(type="string")
     */
    protected $brand;

    /**
     * @ORM\Column(type="string")
     */
    protected $type;

    /**
     * @ORM\Column(type="decimal")
     */
    protected $price;

    /**
     * @ORM\Column(type="integer")
     */
    protected $stock;

    /**
     * Product constructor.
     * @param $reference
     * @param $brand
     * @param $type
     * @param $price
     * @param $stock
     */
    public function __construct($reference, $brand, $type, $price, $stock)
    {
        $this->reference = $reference;
        $this->brand = $brand;
        $this->type = $type;
        $this->price = $price;
        $this->stock = $stock;
        $this->stock = $stock;
    }
}





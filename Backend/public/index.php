<?php

use Doctrine\ORM\Query;
use \Firebase\JWT\JWT;
use Slim\App;

require '../vendor/autoload.php';
require_once '../bootstrap.php';

$config = ['settings' => [
    'addContentLengthHeader' => false,
]];
$app = new App;

const JWT_SECRET = "leoDansLaForetPHP";

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Headers: Content-Type, Authorization");
header("Access-Control-Allow-Methods: HEAD,POST,GET,PUT,PATCH,DELETE");

// Create JWT token
$jwt = new Tuupola\Middleware\JwtAuthentication([
    "secure" => false,
    "path" => ["/secure"],
    "secret" => JWT_SECRET,
    "attribute" => "decoded_token_data",
    "algorithm" => ["HS256"],
    "error" => function ($response)
    {
        $data = array('ERROR' => 'Could not authenticate user.'
        );
        return $response->withHeader("Content-Type", "application/json")->getBody()->write(json_encode($data));
    }
]);

function getAllProducts($request, $response, $args)
{
    global $entityManager;

    $clientRepository = $entityManager->getRepository('Product');

    $query = $clientRepository
        ->createQueryBuilder('c')
        ->getQuery();
    $result = $query->getResult(Query::HYDRATE_ARRAY);

    return $response->withHeader("Content-Type", "application/json")->write(json_encode($result));
}

function getOneProduct($request, $response, $args)
{
    global $entityManager;

    if (isset($args['id']))
    {
        $id = $args['id'];

        $idCurrentProduct = $args['id'];
        $productRepository = $entityManager->getRepository('Product');

        $query = $productRepository
            ->createQueryBuilder('p')
            ->where('p.id = :idCurrentProduct')
            ->setParameter('idCurrentProduct', $idCurrentProduct)
            ->getQuery();
        $result = $query->getResult(Query::HYDRATE_ARRAY);

        return $response->withHeader("Content-Type", "application/json")->write(json_encode($result));
    }
    return $response->withStatus(401);
}

function userSignin($request, $response, $args)
{
    global $entityManager;
    $params = $request->getParsedBody();
    if (isset($params['login']) && isset($params['password']))
    {
        $login = $params['login'];
        $password = $params['password'];

        $UserRepository = $entityManager->getRepository('User');

        $user = $UserRepository->findOneBy(array('login' => $login));

        if ($user != null) {
            if ($user->getPassword() == $password) {
                $issuedAt = time();
                $expirationTime = $issuedAt + 6000; // jwt valid for 60 seconds from the issued time

                $payload = array(
                    'userid' => $user->getId(),
                    'iat' => $issuedAt,
                    'exp' => $expirationTime
                );

                $token_jwt = JWT::encode($payload, JWT_SECRET, "HS256");
                // storage of these data
                $tokendata = array(
                    'success' => true,
                    'jwt_token' => $token_jwt
                );
                // send response
                return $response->withHeader("Content-Type", "application/json")->withJson($tokendata)->withStatus(200);
            }

        }
    }
    return $response->withStatus(401);
}

function signup($request, $response, $args)
{
    global $entityManager;
    $params_valid = true;
    $params = $request->getParsedBody();
    if (!isset($params['login']) || !isset($params['password']) || !isset($params['city'])
        || !isset($params['phone']) || !isset($params['email']) || !isset($params['firstname'])
        || !isset($params['lastname']) || !isset($params['address']) || !isset($params['postalCode'])) {
        $params_valid = false;
    }

    if ($params_valid) {
        // create user
        $newUser = new User($params['login'], $params['password'], $params['email'], $params['phone'],
            $params['firstname'], $params['lastname'], $params['address'], $params['postalCode'], $params['city']);

        $entityManager->persist($newUser);
        $entityManager->flush();

        return $response->withHeader("Content-Type", "application/json")->withJson(json_encode($newUser))->withStatus(201);
    }
    return $response->withHeader("Content-Type", "application/json")->withStatus(401);
}

function getUserById($request, $response, $args)
{
    global $entityManager;

    $params = $request->getParsedBody();
    $token = $request->getAttribute("decoded_token_data")->userid;

    echo($params['jwt_token']);
    if (true)
    {
        $userRepository = $entityManager->getRepository('User');

        $id = 1;

        $query = $userRepository
            ->createQueryBuilder('u')
            ->where('u.id = :idUser')
            ->setParameter('idUser', $id)
            ->getQuery();
        $result = $query->getResult(Query::HYDRATE_ARRAY);

        return $response->withHeader("Content-Type", "application/json")->write(json_encode($result));
    }
    return $response->withStatus(401);
}

$app->group('', function() use ($app) {

    $app->get('/products', 'getAllProducts');
    $app->get('/product/{id}', 'getOneProduct');
    $app->post('/signup', 'signup');
    $app->post('/signin', 'userSignin');
    $app->post('/user', 'getUserById');
});

// Run app
$app->run();

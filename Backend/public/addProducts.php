<?php
require_once './bootstrap.php';

$productRepository = $entityManager->getRepository('User');
$products = $productRepository->findAll();

foreach ($products as $product) {
    echo sprintf("-%s\n", $product->getName());
}
/*
$products = [];

$products = [new Product("#REF2743817", "schleicher", "ASW20", "35000", 2),
    new Product("#REF2743818", "schleicher", "ASW27", "65000", 6),
    new Product("#REF2743819", "schleicher", "ASW29", "130000", 21),
    new Product("#REF2743820", "schempp-hirth", "Ventus 3", "195000", 2),
    new Product("#REF2743821", "schempp-hirth", "Arcus M", "280000", 6)];

foreach ($products as $product) {
    $entityManager->persist($product);
    $entityManager->flush();
}
*/

